To make Water 2D Tool compatible with Unity LWRP, follow the instructions bellow:

1. In the package manager, make sure that you have the latest version of Lightweight RP instaled.
2. Right click on the Assets folder and select Import Package/Custom Package. In the window that pops up, go to where your project is and
   from the Water2D_Tool/Assets/LWRP folder import Water2DTool_v1.10_LWRP.unitypackage