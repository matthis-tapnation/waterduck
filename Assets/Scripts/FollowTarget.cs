﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class FollowTarget : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
            return;
        if (Vector3.Distance(this.transform.position, target.transform.position) > distanceMin)
            navmeshAgent.SetDestination(target.position);
    }
    public void ChangeTarget(Transform newTarget)
    {
        target = newTarget;
    }
    public Transform target;
    public float distanceMin;
    public NavMeshAgent navmeshAgent;
}
